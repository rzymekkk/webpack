function showClockInElement(element) {
    return setInterval(()=>{
        element.innerText = moment().format('HH:mm:ss.S');
    }, 100);
}